'use strict';

angular.module('myApp.showEmployees', ['ngRoute'])

    .config(['$routeProvider', function ($routeProvider) {
        $routeProvider.when('/showemployees', {
            templateUrl: 'views/showEmployees/showEmployees.html',
            controller: 'ShowEmployeesCtrl'
        });
    }])

    .controller('ShowEmployeesCtrl', ['AgencyService', 'BranchService', 'EmployeeService', function (AgencyService, BranchService, EmployeeService) {
        var self = this;
        self.selectedAgencyId = '';
        self.selectedBranchId = '';
        self.currentAgencyName = '';
        self.currentBranchName = '';

        this.chooseAgency = function () {
            AgencyService.getAllAgencies();
        };

        // have agencies in $rootScope.agencies after running this method
        this.chooseAgency();

        this.getBranchesList = function () {
            console.log(self.selectedAgencyId);
            self.currentAgencyName = AgencyService.findById(self.selectedAgencyId);
            console.log(self.currentAgencyName.name);
            BranchService.getAllBranchesWithAgencyId(self.selectedAgencyId);
        };

        this.getEmployeesFromBranch = function () {
           console.log (self.selectedBranchId);
           EmployeeService.getAllEmployeesFromBranchId(self.selectedBranchId);
           self.currentBranchName = BranchService.findBranchById(self.selectedBranchId);
           console.log(self.currentBranchName.name);
        };


    }]);