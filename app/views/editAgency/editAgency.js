'use strict';

angular.module('myApp.editAgency', ['ngRoute'])

    .config(['$routeProvider', function ($routeProvider) {
        $routeProvider.when('/editagency', {
            templateUrl: 'views/editAgency/editAgency.html',
            controller: 'EditAgencyCtrl'
        });
    }])

    .controller('EditAgencyCtrl', ['AgencyService', function (AgencyService) {
        var self = this;

        this.agencyUpdate = {
            'name' : '',
            'description' : '',
            'email' : ''
        };

        this.pobierzAgencje = function () {
            AgencyService.getAllAgencies();
        };
        this.pobierzAgencje();

        this.edytujAgencje = function (agencyId) {
            self.currentAgency = AgencyService.findById(agencyId);
            console.log('wybrana agencja - id: '+ agencyId);
            console.log(self.currentAgency);
        }

        this.zaktualizujAgencje = function () {
            AgencyService.updateAgency(self.currentAgency.id, self.agencyUpdate);
        }

    }]);