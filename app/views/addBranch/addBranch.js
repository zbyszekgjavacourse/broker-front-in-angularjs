'use strict';

angular.module('myApp.addBranch', ['ngRoute'])

    .config(['$routeProvider', function ($routeProvider) {
        $routeProvider.when('/addbranch', {
            templateUrl: 'views/addBranch/addBranch.html',
            controller: 'AddBranchCtrl'
        });
    }])

    .controller('AddBranchCtrl', ['AgencyService', 'BranchService', function (AgencyService, BranchService) {
        var self = this;

        this.branchCreate = {
            'name': '',
            'address': '',
            'managerNameAndSurname': ''
        };

        this.pobierzAgencje = function () {
            AgencyService.getAllAgencies();
        };
        this.pobierzAgencje();

        this.dodajOddzial = function () {
            BranchService.addBranchToAgency(self.selectedId, self.branchCreate);

        };


    }]);