'use strict';

angular.module('myApp.showBranches', ['ngRoute'])

    .config(['$routeProvider', function ($routeProvider) {
        $routeProvider.when('/showbranches', {
            templateUrl: 'views/showBranches/showBranches.html',
            controller: 'ShowBranchesCtrl'
        });
    }])

    .controller('ShowBranchesCtrl', ['AgencyService', 'BranchService', function (AgencyService, BranchService) {
        var self = this;
        this.pobierzAgencje = function () {
            AgencyService.getAllAgencies();
        };
        this.pobierzAgencje();

        this.listujOddzialy = function () {
            BranchService.getAllBranchesWithAgencyId(self.selectedId);
            self.currentAgency = AgencyService.findById(self.selectedId);
            // console.log(self.currentAgency);
        };



    }]);