'use strict';

angular.module('myApp.viewRegister', ['ngRoute'])

    .config(['$routeProvider', function ($routeProvider) {
        $routeProvider.when('/viewregister', {
            templateUrl: 'views/viewRegister/viewRegister.html',
            controller: 'ViewRegisterCtrl'
        });
    }])

    .controller('ViewRegisterCtrl', ['$http', 'AuthService', '$window', function ($http, AuthService, $window) {
        var self = this;
        var URL = "http://localhost:8084";

        this.registerUser = {
            'login': '',
            'name': '',
            'password': ''
        };

        this.sendRegisterForm = function () {
            $http.post(URL + '/auth/register', self.registerUser)
                .then(
                    function (odpowiedz) {
                        console.log(odpowiedz);
                    },
                    function (odpowiedzNaBlad) {
                        console.log(odpowiedzNaBlad);
                    });
        };

    }]);