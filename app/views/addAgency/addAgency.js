'use strict';

angular.module('myApp.addAgency', ['ngRoute'])

    .config(['$routeProvider', function ($routeProvider) {
        $routeProvider.when('/addagency', {
            templateUrl: 'views/addAgency/addAgency.html',
            controller: 'AddAgencyCtrl'
        });
    }])

    .controller('AddAgencyCtrl', ['AgencyService', '$http', function (AgencyService, $http) {
        var self = this;
        var URL = "http://localhost:8084";

        this.agencyCreate = {
            'name': '',
            'description': '',
            'email': ''
        };

        this.dodajAgencje = function () {
            $http.post(URL + '/agencies/', self.agencyCreate)
                .then(
                    function (odpowiedz) {
                        console.log(odpowiedz);
                    },
                    function (odpowiedzNaBlad) {
                        console.log(odpowiedzNaBlad);
                    });
        };

    }]);