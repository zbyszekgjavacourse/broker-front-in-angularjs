'use strict';

angular.module('myApp.showAgencies', ['ngRoute'])

    .config(['$routeProvider', function ($routeProvider) {
        $routeProvider.when('/showagencies', {
            templateUrl: 'views/ShowAgencies/showagencies.html',
            controller: 'ShowAgenciesCtrl'
        });
    }])

    .controller('ShowAgenciesCtrl', ['AgencyService', function (AgencyService) {
        this.pobierzAgencje = function () {
            AgencyService.getAllAgencies();
        };
        this.pobierzAgencje();
    }]);