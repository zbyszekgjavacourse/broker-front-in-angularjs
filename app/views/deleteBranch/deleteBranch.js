'use strict';

angular.module('myApp.deleteBranch', ['ngRoute'])

    .config(['$routeProvider', function ($routeProvider) {
        $routeProvider.when('/deletebranch', {
            templateUrl: 'views/deleteBranch/deleteBranch.html',
            controller: 'DeleteBranchCtrl'
        });
    }])

    .controller('DeleteBranchCtrl', ['AgencyService', 'BranchService', function (AgencyService, BranchService) {
        var self = this;
        this.pobierzAgencje = function () {
            AgencyService.getAllAgencies();
        };
        this.pobierzAgencje();

        this.listujOddzialy = function () {
            BranchService.getAllBranchesWithAgencyId(self.selectedId);
            self.currentAgency = AgencyService.findById(self.selectedId);
            // console.log(self.currentAgency);
        };

        this.usunOddzial = function (branchId) {
            BranchService.deleteBranch(self.selectedId, branchId);
        }

    }]);