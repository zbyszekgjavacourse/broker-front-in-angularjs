'use strict';

angular.module('myApp.editBranch', ['ngRoute'])

    .config(['$routeProvider', function ($routeProvider) {
        $routeProvider.when('/editbranch', {
            templateUrl: 'views/editBranch/editBranch.html',
            controller: 'EditBranchCtrl'
        });
    }])

    .controller('EditBranchCtrl', ['AgencyService', 'BranchService', function (AgencyService, BranchService) {
        var self = this;
        this.choosenBranch = '';
        this.branchUpdate = {
            'name': '',
            'address': '',
            'managerNameAndSurname': ''
        };


        this.pobierzAgencje = function () {
            AgencyService.getAllAgencies();
        };
        this.pobierzAgencje();

        this.listujOddzialy = function () {
            BranchService.getAllBranchesWithAgencyId(self.selectedId);
            self.currentAgency = AgencyService.findById(self.selectedId);
            // console.log(self.currentAgency);
        };

        this.edytujOddzial = function (branchId) {
            self.currentBranch = BranchService.findBranchById(branchId);
            self.choosenBranch = branchId;
            console.log('wybrany oddzial - id: '+ branchId);
        }

        this.zaktualizujOddzial = function () {
            BranchService.updateBranch(self.selectedId, self.choosenBranch, self.branchUpdate);
            }



    }]);