'use strict';

angular.module('myApp.deleteAgency', ['ngRoute'])

    .config(['$routeProvider', function ($routeProvider) {
        $routeProvider.when('/deleteagency', {
            templateUrl: 'views/deleteAgency/deleteAgency.html',
            controller: 'DeleteAgencyCtrl'
        });
    }])

    .controller('DeleteAgencyCtrl', ['AgencyService', function (AgencyService) {
        var self = this;

        this.pobierzAgencje = function () {
            AgencyService.getAllAgencies();
        };
        this.pobierzAgencje();

        this.usunAgencje = function (agencyId) {
            AgencyService.deleteAgency(agencyId);
        }

    }]);