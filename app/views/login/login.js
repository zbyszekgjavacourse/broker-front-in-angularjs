'use strict';

angular.module('myApp.login', ['ngRoute'])

    .config(['$routeProvider', function ($routeProvider) {
        $routeProvider.when('/login', {
            templateUrl: 'views/login/login.html',
            controller: 'LoginCtrl'
        });
    }])

    .controller('LoginCtrl', ['$http', 'AuthService', '$window', function ($http, AuthService, $window) {
        var self = this;
        var URL = "http://localhost:8084";

        this.loginUser = {
            'login': '',
            'password': ''
        };

        this.sendLoginForm = function () {
            $http.post(URL + '/auth/authenticate', self.loginUser)
                .then(
                    function (odpowiedz) {
                        // jeśli się uda
                        console.log(odpowiedz);
                        var token = odpowiedz.data.token;
                        var user_id =odpowiedz.data.id;

                        $http.defaults.headers.common['Authorization'] = 'Bearer ' + token;

                        // token i user_id zapisuje do sesji
                        $window.sessionStorage.setItem('token', token);
                        $window.sessionStorage.setItem('user_id', user_id);

                        // zapamietujemy w AuthService zalogowanego użytkownika
                        AuthService.loggedInUser = user_id;

                    },
                    function (odpowiedzNaBlad) {
                        // nie udało się
                        console.log(odpowiedzNaBlad);
                    });
        };

    }]);