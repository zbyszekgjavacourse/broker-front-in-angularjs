'use strict';

// Declare app level module which depends on views, and components
angular.module('myApp', [
    'ngRoute',
    'myApp.main',
    'myApp.viewRegister',
    'myApp.login',
    'myApp.showAgencies',
    'myApp.addAgency',
    'myApp.editAgency',
    'myApp.deleteAgency',
    'myApp.addBranch',
    'myApp.editBranch',
    'myApp.deleteBranch',
    'myApp.showBranches',
    'myApp.showEmployees',
    'myApp.authService',
    'myApp.agencyService',
    'myApp.branchService',
    'myApp.employeeService',
    'myApp.version'
]).config(['$locationProvider', '$routeProvider', function ($locationProvider, $routeProvider) {
    $locationProvider.hashPrefix('!');

    $routeProvider.otherwise({redirectTo: '/main'});
}]).run(function(AuthService, $http, $window) {
    if (AuthService.loggedInUser === '') {
        // jesśmy niezalogowani
        var token = $window.sessionStorage.getItem('token');
        var user_id = $window.sessionStorage.getItem('user_id');

        if (token == null || token === undefined || user_id == null || user_id === undefined) {
            // to znaczy, że nie jesteśmy zalogowani
            $window.location = '#!/login';
        } else {
            $http.defaults.headers.common['Authorization'] = 'Bearer ' + token;
            AuthService.loggedInUser = user_id;
        }
    }

});
