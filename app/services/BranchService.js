'use strict';

angular.module('myApp.branchService', ['ngRoute'])
    .service('BranchService', ['$http', '$rootScope', function ($http, $rootScope) {
        var URL = "http://localhost:8084"
        this.addBranchToAgency = addBranchToAgency;
        this.getAllBranchesWithAgencyId = getAllBranchesWithAgencyId;
        this.findBranchById = findBranchById;
        this.updateBranch = updateBranch;
        this.deleteBranch = deleteBranch;

        function addBranchToAgency(agencyId, params) {
            $http.post(URL + '/agencies/' + agencyId + '/branches/', params)
                .then(function (odpowiedz) {
                    console.log(odpowiedz.data);
                }, function (odpowiedzKiedyBlad) {
                    console.log(odpowiedzKiedyBlad);
                });
        }

        function getAllBranchesWithAgencyId(agencyId) {
            $http.get(URL + '/agencies/' + agencyId + '/branches')
                .then(function (odpowiedz) {
                    console.log(odpowiedz.data);
                    $rootScope.branches = [];
                    for (var index in odpowiedz.data.content) {
                        var branch = odpowiedz.data.content[index];
                        branch.createdAt = branch.createdAt.split('T')[0] + ' '+ branch.createdAt.split('T')[1].split('.')[0].substring(0, branch.createdAt.split('T')[1].split('.')[0].length-3);
                        branch.updatedAt = branch.updatedAt.split('T')[0] + ' '+ branch.updatedAt.split('T')[1].split('.')[0].substring(0, branch.updatedAt.split('T')[1].split('.')[0].length-3);
                        $rootScope.branches.push(odpowiedz.data.content[index]);
                    }
                }, function (odpowiedzKiedyBlad) {
                    console.log(odpowiedzKiedyBlad);
                });
        }

        function findBranchById(id) {
            for(var i = 0; i< $rootScope.branches.length; i++){
                console.log($rootScope.branches[i]);
                if ($rootScope.branches[i].id == id){
                    return $rootScope.branches[i];
                }
            }
            return null;
        }

        /*@PutMapping ("/agencies/{agencyId}/branches/{branchId}")*/
        function updateBranch(agencyId, branchId, updatedBranch) {
            $http.put(URL + '/agencies/' + agencyId +'/branches/' + branchId, updatedBranch)
                .then(function (odpowiedz) {
                    console.log(odpowiedz.data);

            }, function(odpowiedzKiedyBlad) {
                    console.log(odpowiedzKiedyBlad);
            });
        }

        /*@DeleteMapping ("/agencies/{agencyId}/branches/{branchId}")*/
        function deleteBranch(agencyId, branchId) {
            $http.delete(URL + '/agencies/' + agencyId +'/branches/' + branchId)
                .then(function (odpowiedz) {
                    console.log(odpowiedz.data);

                }, function(odpowiedzKiedyBlad) {
                    console.log(odpowiedzKiedyBlad);
                });
        }


    }]);