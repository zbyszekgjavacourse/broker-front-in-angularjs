'use strict';

angular.module('myApp.employeeService', ['ngRoute'])
    .service('EmployeeService', ['$http', '$rootScope', function ($http, $rootScope) {
        var URL = "http://localhost:8084";
        this.getAllEmployeesFromBranchId = getAllEmployeesFromBranchId;

        /*@GetMapping ("branches/{branchId}/employees")*/
        function getAllEmployeesFromBranchId(branchId) {
            $http.get(URL + '/branches/' + branchId + '/employees')
                .then(function (odpowiedz) {
                    console.log(odpowiedz.data);
                    $rootScope.employees = [];
                    for (var index in odpowiedz.data.content) {
                        var employee = odpowiedz.data.content[index];
                        employee.createdAt = employee.createdAt.split('T')[0] + ' '+ employee.createdAt.split('T')[1].split('.')[0].substring(0, employee.createdAt.split('T')[1].split('.')[0].length-3);
                        employee.updatedAt = employee.updatedAt.split('T')[0] + ' '+ employee.updatedAt.split('T')[1].split('.')[0].substring(0, employee.updatedAt.split('T')[1].split('.')[0].length-3);
                        $rootScope.employees.push(odpowiedz.data.content[index]);
                    }
                }, function (odpowiedzKiedyBlad) {
                    console.log(odpowiedzKiedyBlad);
                });
        }


    }]);