'use strict';

angular.module('myApp.agencyService', ['ngRoute'])
    .service('AgencyService', ['$http', '$rootScope', function ($http, $rootScope) {
        var URL = "http://localhost:8084";
        this.getAllAgencies = getAllAgencies;
        this.findById = findById;
        this.updateAgency = updateAgency;
        this.deleteAgency = deleteAgency;

        function getAllAgencies() {
            $http.get(URL + '/agencies')
                .then(function (odpowiedz) {
                    $rootScope.agencies = [];
                    for (var index in odpowiedz.data.content) {
                        var agency = odpowiedz.data.content[index];
                        agency.createdAt = agency.createdAt.split('T')[0] + ' ' + agency.createdAt.split('T')[1].split('.')[0].substring(0, agency.createdAt.split('T')[1].split('.')[0].length - 3);
                        agency.updatedAt = agency.updatedAt.split('T')[0] + ' ' + agency.updatedAt.split('T')[1].split('.')[0].substring(0, agency.updatedAt.split('T')[1].split('.')[0].length - 3);
                        $rootScope.agencies.push(agency);
                    }
                    console.log(odpowiedz.data);
                }, function (odpowiedzKiedyBlad) {
                    console.log(odpowiedzKiedyBlad);
                });
        }

        function findById(id) {
            for (var i = 0; i < $rootScope.agencies.length; i++) {
                // console.log($rootScope.agencies[i]);
                if ($rootScope.agencies[i].id == id) {
                    return $rootScope.agencies[i];
                }
            }
            return null;
        }

        /*@PutMapping ("/agencies/{agencyId}")*/
        function updateAgency(agencyId, agency) {
            $http.put(URL + '/agencies/' + agencyId, agency)
                .then(function (odpowiedz) {
                    console.log(odpowiedz.data);
                }, function (odpowiedzKiedyBlad) {
                    console.log(odpowiedzKiedyBlad);
                });
        }

        /*@DeleteMapping ("/agencies/{agencyId}")*/
        function deleteAgency(agencyId) {
            $http.delete(URL + '/agencies/' + agencyId)
                .then(function (odpowiedz) {
                    console.log(odpowiedz.data);
                }, function (odpowiedzKiedyBlad) {
                    console.log(odpowiedzKiedyBlad);
                });
        }

    }]);